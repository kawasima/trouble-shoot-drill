package net.unit8.example.product.application.impl;

import net.unit8.example.product.application.SearchProductsCommand;
import net.unit8.example.product.application.SearchProductsUseCase;
import net.unit8.example.product.application.SearchProjectsPort;
import net.unit8.example.product.domain.FoundProductsEvent;
import net.unit8.example.product.domain.Product;
import net.unit8.example.stereotype.UseCase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@UseCase
class SearchProductsUseCaseImpl implements SearchProductsUseCase {
    private final SearchProjectsPort searchProjectsPort;

    public SearchProductsUseCaseImpl(SearchProjectsPort searchProjectsPort) {
        this.searchProjectsPort = searchProjectsPort;
    }

    @Override
    public FoundProductsEvent handle(SearchProductsCommand command) {
        Page<Product> products = searchProjectsPort
                .search(command.getQuery(), PageRequest.of(command.getPage(), command.getLimit()));

        return new FoundProductsEvent(products, command.getQuery());
    }
}
