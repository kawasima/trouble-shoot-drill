package net.unit8.example.product.application;

import net.unit8.example.product.domain.FoundProductsEvent;

public interface SearchProductsUseCase {
    FoundProductsEvent handle(SearchProductsCommand command);
}
