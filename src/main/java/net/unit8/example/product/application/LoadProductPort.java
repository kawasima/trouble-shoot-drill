package net.unit8.example.product.application;

import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;

public interface LoadProductPort {
    Product load(ProductId productId);
}
