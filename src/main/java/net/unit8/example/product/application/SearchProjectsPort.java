package net.unit8.example.product.application;

import net.unit8.example.product.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SearchProjectsPort {
    Page<Product> search(String query, Pageable pageable);
}
