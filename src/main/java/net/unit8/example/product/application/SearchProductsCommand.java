package net.unit8.example.product.application;

import lombok.Value;

import java.io.Serializable;

@Value
public class SearchProductsCommand implements Serializable {
    String query;
    int page;
    int limit;
}
