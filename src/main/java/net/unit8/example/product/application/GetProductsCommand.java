package net.unit8.example.product.application;

import lombok.Value;
import net.unit8.example.product.domain.ProductId;

import java.util.Set;

@Value
public class GetProductsCommand {
    Set<ProductId> productIds;
}
