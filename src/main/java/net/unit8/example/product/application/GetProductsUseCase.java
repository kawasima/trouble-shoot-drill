package net.unit8.example.product.application;

import net.unit8.example.product.domain.GotProductsEvent;

public interface GetProductsUseCase {
    GotProductsEvent handle(GetProductsCommand command);
}
