package net.unit8.example.product.application.impl;

import net.unit8.example.product.application.GetProductsCommand;
import net.unit8.example.product.application.GetProductsUseCase;
import net.unit8.example.product.application.LoadProductsPort;
import net.unit8.example.product.domain.GotProductsEvent;
import net.unit8.example.product.domain.Product;
import net.unit8.example.stereotype.UseCase;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Set;

@UseCase
class GetProductsUseCaseImpl implements GetProductsUseCase {
    private final LoadProductsPort loadProductsPort;
    private final TransactionTemplate tx;

    GetProductsUseCaseImpl(LoadProductsPort loadProductsPort, TransactionTemplate tx) {
        this.loadProductsPort = loadProductsPort;
        this.tx = tx;
    }

    @Override
    public GotProductsEvent handle(GetProductsCommand command) {
        tx.setReadOnly(true);
        return tx.execute(status -> {
            Set<Product> products = loadProductsPort.loadProducts(command.getProductIds());
            return new GotProductsEvent(products);
        });
    }
}
