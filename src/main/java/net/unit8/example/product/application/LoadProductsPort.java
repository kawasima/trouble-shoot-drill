package net.unit8.example.product.application;

import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;

import java.util.Set;

public interface LoadProductsPort {
    Set<Product> loadProducts(Set<ProductId> productIds);
}
