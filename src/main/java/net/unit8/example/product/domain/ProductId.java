package net.unit8.example.product.domain;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Value;

@Value
public class ProductId {
    @JsonValue
    Long value;
}
