package net.unit8.example.product.domain;

import lombok.Value;

import java.util.Set;

@Value
public class GotProductsEvent {
    Set<Product> products;
}
