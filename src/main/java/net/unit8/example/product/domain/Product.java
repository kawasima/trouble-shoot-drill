package net.unit8.example.product.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor(staticName = "withId")
@RequiredArgsConstructor(staticName = "withoutId")
@Getter
public class Product {
    private ProductId id;

    private final String name;
    private final BigDecimal price;
}
