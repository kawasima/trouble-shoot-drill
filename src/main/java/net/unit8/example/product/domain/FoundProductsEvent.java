package net.unit8.example.product.domain;

import lombok.Value;
import org.springframework.data.domain.Page;

import java.io.Serializable;

@Value
public class FoundProductsEvent implements Serializable {
    Page<Product> products;
    String query;
}
