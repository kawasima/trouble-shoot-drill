package net.unit8.example.product.adapter.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;
import java.util.stream.Stream;

public interface ProductRepository extends JpaRepository<ProductJpaEntity, Long> {
    @Query("SELECT p FROM ProductJpaEntity p WHERE p.name like :query")
    Page<ProductJpaEntity> findAllByQuery(@Param("query") String query, Pageable pageable);

    @Query("SELECT p FROM ProductJpaEntity p WHERE p.id in :ids")
    Stream<ProductJpaEntity> findAllByProductIds(@Param("ids") Set<Long> ids);
}
