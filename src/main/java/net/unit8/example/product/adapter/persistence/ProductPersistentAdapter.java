package net.unit8.example.product.adapter.persistence;

import net.unit8.example.product.application.LoadProductsPort;
import net.unit8.example.product.application.SearchProjectsPort;
import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.stereotype.PersistenceAdapter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;
import java.util.stream.Collectors;

@PersistenceAdapter
public class ProductPersistentAdapter implements SearchProjectsPort, LoadProductsPort {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductPersistentAdapter(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public Page<Product> search(String query, Pageable pageable) {
        if (query == null) {
            return productRepository.findAll(pageable)
                    .map(productMapper::entityToDomain);
        } else {
            return productRepository.findAllByQuery("%" + query + "%", pageable)
                    .map(productMapper::entityToDomain);
        }
    }

    @Override
    public Set<Product> loadProducts(Set<ProductId> productIds) {
        return productRepository.findAllByProductIds(productIds.stream()
                .map(ProductId::getValue)
                .collect(Collectors.toSet()))
                .map(productMapper::entityToDomain)
                .collect(Collectors.toSet());
    }
}
