package net.unit8.example.product.adapter.persistence;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "products")
@Data
public class ProductJpaEntity implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private BigDecimal price;
}
