package net.unit8.example.product.adapter.persistence;

import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class ProductMapper {
    ProductJpaEntity domainToEntity(Product product) {
        ProductJpaEntity entity = new ProductJpaEntity();
        Optional.ofNullable(product.getId())
                .ifPresent(id -> entity.setId(id.getValue()));
        entity.setName(product.getName());
        entity.setPrice(product.getPrice());
        return entity;
    }

    Product entityToDomain(ProductJpaEntity entity) {
        return Product.withId(new ProductId(entity.getId()),
                entity.getName(),
                entity.getPrice()
                );
    }
}
