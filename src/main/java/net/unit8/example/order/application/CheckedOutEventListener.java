package net.unit8.example.order.application;

import net.unit8.example.cart.domain.CheckedOutEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class CheckedOutEventListener {
    private final PlaceOrderUseCase placeOrderUseCase;

    public CheckedOutEventListener(PlaceOrderUseCase placeOrderUseCase) {
        this.placeOrderUseCase = placeOrderUseCase;
    }

    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void onCheckedOut(CheckedOutEvent event) {

    }
}
