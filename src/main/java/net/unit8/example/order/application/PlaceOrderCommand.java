package net.unit8.example.order.application;

import lombok.Value;
import net.unit8.example.user.domain.MemberId;

import java.util.Set;

@Value
public class PlaceOrderCommand {
    MemberId memberId;
    Set<PlaceOrderLineCommand> placeOrderLines;
}
