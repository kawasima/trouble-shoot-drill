package net.unit8.example.order.application;

import net.unit8.example.order.domain.Order;

public interface SaveOrderPort {
    Order save(Order order);
}
