package net.unit8.example.order.application;

import lombok.Value;
import net.unit8.example.user.domain.MemberId;

@Value
public class QueryOrdersCommand {
    MemberId memberId;
}
