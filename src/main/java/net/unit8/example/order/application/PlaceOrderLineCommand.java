package net.unit8.example.order.application;

import lombok.Value;
import net.unit8.example.product.domain.ProductId;

@Value
public class PlaceOrderLineCommand {
    ProductId productId;
    int amount;
}
