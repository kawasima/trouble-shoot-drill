package net.unit8.example.order.application;

import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.fn.Either;
import net.unit8.example.order.domain.PlacedOrderEvent;

public interface PlaceOrderUseCase {
    Either<ConstraintViolations, PlacedOrderEvent> order(PlaceOrderCommand command);
}
