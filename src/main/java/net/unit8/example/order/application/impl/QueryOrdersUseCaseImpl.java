package net.unit8.example.order.application.impl;

import net.unit8.example.order.application.LoadOrdersPort;
import net.unit8.example.order.application.QueryOrdersCommand;
import net.unit8.example.order.application.QueryOrdersUseCase;
import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.QueriedOrdersEvent;
import net.unit8.example.stereotype.UseCase;

import java.util.List;

@UseCase
class QueryOrdersUseCaseImpl implements QueryOrdersUseCase {
    private final LoadOrdersPort loadOrdersPort;

    QueryOrdersUseCaseImpl(LoadOrdersPort loadOrdersPort) {
        this.loadOrdersPort = loadOrdersPort;
    }

    @Override
    public QueriedOrdersEvent handle(QueryOrdersCommand command) {
        List<Order> orders = loadOrdersPort.loadOrders(command.getMemberId());
        return new QueriedOrdersEvent(orders);
    }
}
