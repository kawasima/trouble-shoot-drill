package net.unit8.example.order.application;

import net.unit8.example.order.domain.QueriedOrdersEvent;

public interface QueryOrdersUseCase {
    QueriedOrdersEvent handle(QueryOrdersCommand command);
}
