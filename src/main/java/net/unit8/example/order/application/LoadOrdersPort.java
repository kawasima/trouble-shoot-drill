package net.unit8.example.order.application;

import net.unit8.example.order.domain.Order;
import net.unit8.example.user.domain.MemberId;

import java.util.List;

public interface LoadOrdersPort {
    List<Order> loadOrders(MemberId memberId);
}
