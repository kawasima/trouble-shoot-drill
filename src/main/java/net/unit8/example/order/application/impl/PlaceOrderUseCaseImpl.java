package net.unit8.example.order.application.impl;

import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.fn.Either;
import net.unit8.example.order.application.PlaceOrderCommand;
import net.unit8.example.order.application.PlaceOrderLineCommand;
import net.unit8.example.order.application.PlaceOrderUseCase;
import net.unit8.example.order.application.SaveOrderPort;
import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.OrderLine;
import net.unit8.example.order.domain.PlacedOrderEvent;
import net.unit8.example.product.application.LoadProductsPort;
import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.stereotype.UseCase;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@UseCase
public class PlaceOrderUseCaseImpl implements PlaceOrderUseCase {
    private final SaveOrderPort saveOrderPort;
    private final LoadProductsPort loadProductsPort;
    private final TransactionTemplate tx;

    public PlaceOrderUseCaseImpl(SaveOrderPort saveOrderPort, LoadProductsPort loadProductsPort, TransactionTemplate tx) {
        this.saveOrderPort = saveOrderPort;
        this.loadProductsPort = loadProductsPort;
        this.tx = tx;
    }

    @Override
    public Either<ConstraintViolations, PlacedOrderEvent> order(PlaceOrderCommand command) {
        Set<ProductId> productIds = command.getPlaceOrderLines().stream()
                .map(PlaceOrderLineCommand::getProductId).collect(Collectors.toSet());

        tx.setReadOnly(true);
        Map<ProductId, Product> productsInOrder = tx.execute(status ->
                loadProductsPort.loadProducts(productIds)
                        .stream().collect(Collectors.toMap(Product::getId, p -> p))
        );

        Either<ConstraintViolations, Order> orderResult = Order.withoutId(command.getMemberId(),
                command.getPlaceOrderLines().stream()
                        .map(line -> OrderLine.withoutId(line.getProductId(),
                                productsInOrder.get(line.getProductId()).getName(),
                                line.getAmount(),
                                productsInOrder.get(line.getProductId()).getPrice()))
                        .collect(Collectors.toSet()));

        tx.setReadOnly(false);
        return orderResult.rightMap(order -> tx.execute(status -> {
            Order savedOrder = saveOrderPort.save(order);
            return new PlacedOrderEvent(savedOrder.getId(), savedOrder.getOrderLines());
        }));
    }
}
