package net.unit8.example.order.domain;

import lombok.Value;

@Value
public class OrderLineId {
    Long value;
}
