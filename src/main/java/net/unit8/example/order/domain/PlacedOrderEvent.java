package net.unit8.example.order.domain;

import lombok.Value;

import java.util.Set;

@Value
public class PlacedOrderEvent {
    OrderId orderId;
    Set<OrderLine> orderLines;
}
