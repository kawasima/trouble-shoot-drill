package net.unit8.example.order.domain;

import lombok.Value;

import java.util.List;

@Value
public class QueriedOrdersEvent {
    List<Order> orders;
}
