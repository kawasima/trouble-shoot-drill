package net.unit8.example.order.domain;

import am.ik.yavi.builder.ValidatorBuilder;
import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.core.Validator;
import am.ik.yavi.fn.Either;
import lombok.Getter;
import net.unit8.example.user.domain.MemberId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
public class Order {
    private static final Validator<Order> validator = ValidatorBuilder.<Order>of()
            .constraint(Order::getTotalPrice, "totalPrice", c -> c.greaterThan(BigDecimal.ZERO))
            .build();

    private OrderId id;
    private final MemberId memberId;
    private final Set<OrderLine> orderLines;
    private final LocalDateTime orderedAt = LocalDateTime.now();

    private Order(OrderId id, MemberId memberId, Set<OrderLine> orderLines) {
        this.id = id;
        this.memberId = memberId;
        this.orderLines = orderLines;
    }

    public static Order withId(OrderId id, MemberId memberId, Set<OrderLine> orderLines) {
        return new Order(id, memberId, orderLines);
    }

    public static Either<ConstraintViolations, Order> withoutId(MemberId memberId, Set<OrderLine> orderLines) {
        Order order = new Order(null, memberId, orderLines);
        return validator.validateToEither(order);
    }

    public BigDecimal getTotalPrice() {
        return orderLines.stream()
                .map(OrderLine::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
