package net.unit8.example.order.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.unit8.example.product.domain.ProductId;

import java.math.BigDecimal;

@AllArgsConstructor(staticName = "withId")
@RequiredArgsConstructor(staticName = "withoutId")
@Getter
public class OrderLine {
    private OrderLineId orderLineId;
    private final ProductId productId;
    private final String name;
    private final int amount;
    private final BigDecimal price;

    public BigDecimal getTotalPrice() {
        return price.multiply(BigDecimal.valueOf(amount));
    }
}
