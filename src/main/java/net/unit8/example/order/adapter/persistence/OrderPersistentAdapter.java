package net.unit8.example.order.adapter.persistence;

import net.unit8.example.order.application.LoadOrdersPort;
import net.unit8.example.order.application.SaveOrderPort;
import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.OrderId;
import net.unit8.example.stereotype.PersistenceAdapter;
import net.unit8.example.user.domain.MemberId;

import java.util.List;
import java.util.stream.Collectors;

@PersistenceAdapter
public class OrderPersistentAdapter implements SaveOrderPort, LoadOrdersPort {
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderPersistentAdapter(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public Order save(Order order) {
        OrderJpaEntity entity = orderRepository.saveAndFlush(orderMapper.domainToEntity(order));
        return orderMapper.entityToDomain(entity);
    }

    public Order load(OrderId orderId) {
        return orderRepository.findById(orderId.getValue())
                .map(orderMapper::entityToDomain)
                .orElseThrow();
    }

    @Override
    public List<Order> loadOrders(MemberId memberId) {
        return orderRepository.findAllByMemberId(memberId.getValue())
                .stream()
                .map(orderMapper::entityToDomain)
                .collect(Collectors.toList());
    }
}
