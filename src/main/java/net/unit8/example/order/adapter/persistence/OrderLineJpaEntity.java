package net.unit8.example.order.adapter.persistence;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "order_lines")
@Data
public class OrderLineJpaEntity implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderJpaEntity order;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_name")
    private String name;

    @Column(name = "unit_of_price")
    private BigDecimal unitOfPrice;

    @Column(name = "amount")
    private Long amount;
}
