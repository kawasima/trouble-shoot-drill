package net.unit8.example.order.adapter.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderJpaEntity, Long> {
    @Query("SELECT o FROM OrderJpaEntity o LEFT JOIN FETCH o.orderLines ol "
            + "WHERE o.memberId = :memberId "
            + "ORDER BY o.orderedAt DESC")
    List<OrderJpaEntity> findAllByMemberId(@Param("memberId") Long memberId);
}
