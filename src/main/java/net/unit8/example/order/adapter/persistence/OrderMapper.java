package net.unit8.example.order.adapter.persistence;

import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.OrderId;
import net.unit8.example.user.domain.MemberId;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
class OrderMapper {
    private final OrderLineMapper orderLineMapper;

    OrderMapper(OrderLineMapper orderLineMapper) {
        this.orderLineMapper = orderLineMapper;
    }

    public OrderJpaEntity domainToEntity(Order order) {
        OrderJpaEntity entity = new OrderJpaEntity();
        Optional.ofNullable(order.getId())
                .ifPresent(id -> entity.setId(id.getValue()));

        entity.setOrderedAt(order.getOrderedAt());
        entity.setMemberId(order.getMemberId().getValue());
        entity.setOrderLines(order.getOrderLines().stream()
                .map(orderLineMapper::domainToEntity)
                .collect(Collectors.toSet()));
        return entity;
    }

    public Order entityToDomain(OrderJpaEntity entity) {
        return Order.withId(
                new OrderId(entity.getId()),
                new MemberId(entity.getMemberId()),
                entity.getOrderLines().stream()
                        .map(orderLineMapper::entityToDomain)
                .collect(Collectors.toSet())
        );
    }
}
