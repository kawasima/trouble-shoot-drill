package net.unit8.example.order.adapter.persistence;

import net.unit8.example.order.domain.OrderLine;
import net.unit8.example.order.domain.OrderLineId;
import net.unit8.example.product.domain.ProductId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class OrderLineMapper {
    public OrderLineJpaEntity domainToEntity(OrderLine orderLine) {
        OrderLineJpaEntity entity = new OrderLineJpaEntity();
        Optional.ofNullable(orderLine.getOrderLineId())
                .ifPresent(id -> entity.setId(id.getValue()));
        entity.setProductId(orderLine.getProductId().getValue());
        entity.setName(orderLine.getName());
        entity.setAmount((long) orderLine.getAmount());
        entity.setUnitOfPrice(orderLine.getPrice());
        return entity;
    }

    public OrderLine entityToDomain(OrderLineJpaEntity entity) {
        return OrderLine.withId(
                new OrderLineId(entity.getId()),
                new ProductId(entity.getProductId()),
                entity.getName(),
                entity.getAmount().intValue(),
                entity.getUnitOfPrice()
        );
    }
}
