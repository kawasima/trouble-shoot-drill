package net.unit8.example.order.adapter.persistence;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "orders")
@Data
public class OrderJpaEntity implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "member_id")
    private Long memberId;

    @Column(name = "ordered_at")
    private LocalDateTime orderedAt;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<OrderLineJpaEntity> orderLines;
}
