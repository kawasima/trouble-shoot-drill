package net.unit8.example.cart.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.unit8.example.product.domain.ProductId;

@AllArgsConstructor(staticName = "withId")
@Getter
public class CartItem {
    CartItemId id;
    final ProductId productId;
    final String name;
    int amount;

    public static CartItem withoutId(ProductId productId, String name, int amount) {
        return new CartItem(null, productId, name, amount);
    }

    public void incrementAmount(int amount) {
        this.amount += amount;
    }
}
