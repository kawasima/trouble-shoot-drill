package net.unit8.example.cart.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import net.unit8.example.product.domain.ProductId;

@Value
public class AddedItemEvent {
    @JsonProperty("product_id")
    ProductId productId;
    @JsonProperty("add_amount")
    int addedAmount;
    @JsonProperty("total_amount")
    int totalAmount;
}
