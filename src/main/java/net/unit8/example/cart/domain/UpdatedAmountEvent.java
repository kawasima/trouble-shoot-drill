package net.unit8.example.cart.domain;

import lombok.Value;

import java.util.Set;

@Value
public class UpdatedAmountEvent {
    Set<CartItem> items;
}
