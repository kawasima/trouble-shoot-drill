package net.unit8.example.cart.domain;

import lombok.*;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor(staticName = "withId")
public class Cart {
    @Getter
    CartId id;

    @Getter
    final MemberId memberId;

    @Getter
    final Set<CartItem> items;

    public static Cart withoutId(MemberId memberId) {
        return Cart.withId(null, memberId, new HashSet<>());
    }

    public void addItem(ProductId productId, String name, int amount) {
        items.stream()
                .filter(item -> item.getProductId().equals(productId))
                .findAny()
                .ifPresentOrElse(
                        item -> item.incrementAmount(amount),
                        ()-> items.add(CartItem.withoutId(productId, name, amount))
                );
    }

    public void removeItem(ProductId productId) {
        items.removeIf(item -> item.getProductId().equals(productId));
    }

    public Optional<CartItem> getItem(ProductId productId) {
        return items.stream()
                .filter(item -> item.getProductId().equals(productId))
                .findAny();
    }
}
