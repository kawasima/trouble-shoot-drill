package net.unit8.example.cart.domain;

import lombok.Value;
import net.unit8.example.user.domain.MemberId;

import java.util.Set;

@Value
public class GotCartEvent {
    MemberId memberId;
    Set<CartItem> items;
    int totalCount;
}
