package net.unit8.example.cart.domain;

import lombok.Value;

@Value
public class CartItemId {
    Long value;
}
