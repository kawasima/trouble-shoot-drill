package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.Cart;
import net.unit8.example.user.domain.MemberId;

public interface LoadCartPort {
    Cart load(MemberId memberId);
}
