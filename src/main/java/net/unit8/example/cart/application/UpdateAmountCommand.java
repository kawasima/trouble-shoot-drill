package net.unit8.example.cart.application;

import lombok.Value;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Value
public class UpdateAmountCommand {
    MemberId memberId;
    Set<AmountForUpdate> setOfAmountForUpdate;

    public static UpdateAmountCommand create(MemberId memberId, AmountForUpdate... setOfAmountForUpdate) {
        return new UpdateAmountCommand(memberId, new HashSet<>(Arrays.asList(setOfAmountForUpdate)));
    }

    public static AmountForUpdate amount(ProductId productId, int amount) {
        return new AmountForUpdate(productId, amount);
    }

    @Value
    public static class AmountForUpdate {
        ProductId productId;
        int amount;
    }
}
