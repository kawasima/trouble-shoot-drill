package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.AddedItemEvent;

public interface AddItemUseCase {
    AddedItemEvent handle(AddItemCommand command);
}
