package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.CheckedOutEvent;

public interface CheckOutUseCase {
    CheckedOutEvent handle(CheckOutCommand command);
}
