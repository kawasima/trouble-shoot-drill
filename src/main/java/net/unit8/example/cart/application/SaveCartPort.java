package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.Cart;

public interface SaveCartPort {
    Cart save(Cart cart);
}
