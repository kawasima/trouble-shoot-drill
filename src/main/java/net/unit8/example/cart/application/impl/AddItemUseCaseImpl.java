package net.unit8.example.cart.application.impl;

import net.unit8.example.cart.application.AddItemCommand;
import net.unit8.example.cart.application.AddItemUseCase;
import net.unit8.example.cart.application.LoadCartPort;
import net.unit8.example.cart.application.SaveCartPort;
import net.unit8.example.cart.domain.AddedItemEvent;
import net.unit8.example.cart.domain.Cart;
import net.unit8.example.cart.domain.CartItem;
import net.unit8.example.stereotype.UseCase;
import org.springframework.transaction.support.TransactionTemplate;

@UseCase
class AddItemUseCaseImpl implements AddItemUseCase {
    private final LoadCartPort loadCartPort;
    private final SaveCartPort saveCartPort;
    private final TransactionTemplate tx;

    public AddItemUseCaseImpl(LoadCartPort loadCartPort, SaveCartPort saveCartPort, TransactionTemplate tx) {
        this.loadCartPort = loadCartPort;
        this.saveCartPort = saveCartPort;
        this.tx = tx;
    }

    @Override
    public AddedItemEvent handle(AddItemCommand command) {
        Cart cart = loadCartPort.load(command.getMemberId());
        cart.addItem(command.getProductId(), command.getName(), command.getAmount());
        tx.execute(status -> {
            saveCartPort.save(cart);
            return null;
        });

        return new AddedItemEvent(
                command.getProductId(),
                command.getAmount(),
                cart.getItem(command.getProductId())
                        .map(CartItem::getAmount)
                        .orElseThrow()
        );
    }
}
