package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.GotCartEvent;

public interface GetCartUseCase {
    GotCartEvent handle(GetCartCommand command);
}
