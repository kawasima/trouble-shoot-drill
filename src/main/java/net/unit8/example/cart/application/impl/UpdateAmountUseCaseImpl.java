package net.unit8.example.cart.application.impl;

import net.unit8.example.cart.application.LoadCartPort;
import net.unit8.example.cart.application.SaveCartPort;
import net.unit8.example.cart.application.UpdateAmountCommand;
import net.unit8.example.cart.application.UpdateAmountUseCase;
import net.unit8.example.cart.domain.Cart;
import net.unit8.example.cart.domain.UpdatedAmountEvent;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.stereotype.UseCase;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.HashSet;
import java.util.Set;

@UseCase
class UpdateAmountUseCaseImpl implements UpdateAmountUseCase {
    private final LoadCartPort loadCartPort;
    private final SaveCartPort saveCartPort;
    private final TransactionTemplate tx;

    public UpdateAmountUseCaseImpl(LoadCartPort loadCartPort, SaveCartPort saveCartPort, TransactionTemplate tx) {
        this.loadCartPort = loadCartPort;
        this.saveCartPort = saveCartPort;
        this.tx = tx;
    }

    @Override
    public UpdatedAmountEvent handle(UpdateAmountCommand command) {
        Cart cart = loadCartPort.load(command.getMemberId());

        Set<ProductId> removeProductIds = new HashSet<>();
        command.getSetOfAmountForUpdate().forEach(afu -> {
            cart.getItem(afu.getProductId())
                    .ifPresent(item -> {
                        if (afu.getAmount() > 0) {
                            int diff = afu.getAmount() - item.getAmount();
                            item.incrementAmount(diff);
                        } else {
                            removeProductIds.add(item.getProductId());
                        }
                    });
        });
        removeProductIds.forEach(cart::removeItem);
        return tx.execute(status -> {
            Cart savedCart = saveCartPort.save(cart);
            return new UpdatedAmountEvent(
                    savedCart.getItems()
            );
        });
    }
}
