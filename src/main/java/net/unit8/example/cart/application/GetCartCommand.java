package net.unit8.example.cart.application;

import lombok.Value;
import net.unit8.example.user.domain.MemberId;

@Value
public class GetCartCommand {
    MemberId memberId;
}
