package net.unit8.example.cart.application;

import lombok.Value;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;

@Value
public class AddItemCommand {
    MemberId memberId;
    ProductId productId;
    String name;
    int amount;
}
