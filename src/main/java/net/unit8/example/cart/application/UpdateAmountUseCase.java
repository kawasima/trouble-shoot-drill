package net.unit8.example.cart.application;

import net.unit8.example.cart.domain.UpdatedAmountEvent;

public interface UpdateAmountUseCase {
    UpdatedAmountEvent handle(UpdateAmountCommand command);
}
