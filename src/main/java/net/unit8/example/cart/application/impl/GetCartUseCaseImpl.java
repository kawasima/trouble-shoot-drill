package net.unit8.example.cart.application.impl;

import net.unit8.example.cart.application.GetCartCommand;
import net.unit8.example.cart.application.GetCartUseCase;
import net.unit8.example.cart.application.LoadCartPort;
import net.unit8.example.cart.domain.Cart;
import net.unit8.example.cart.domain.GotCartEvent;
import net.unit8.example.stereotype.UseCase;

@UseCase
class GetCartUseCaseImpl implements GetCartUseCase {
    private final LoadCartPort loadCartPort;

    public GetCartUseCaseImpl(LoadCartPort loadCartPort) {
        this.loadCartPort = loadCartPort;
    }

    @Override
    public GotCartEvent handle(GetCartCommand command) {
        Cart cart = loadCartPort.load(command.getMemberId());
        return new GotCartEvent(
                command.getMemberId(),
                cart.getItems(),
                cart.getItems().stream()
                        .mapToInt(item -> item.getAmount())
                        .sum()
        );
    }
}
