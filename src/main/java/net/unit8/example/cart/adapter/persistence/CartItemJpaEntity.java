package net.unit8.example.cart.adapter.persistence;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cart_items")
@Getter
@Setter
@ToString
public class CartItemJpaEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @ToString.Exclude
    private CartJpaEntity cart;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "name")
    private String name;

    private int amount;

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        } else {
            return super.hashCode();
        }
    }
}
