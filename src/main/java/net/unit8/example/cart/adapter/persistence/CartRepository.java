package net.unit8.example.cart.adapter.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartRepository extends JpaRepository<CartJpaEntity, Long> {
    Optional<CartJpaEntity> findByUserId(Long userId);
}
