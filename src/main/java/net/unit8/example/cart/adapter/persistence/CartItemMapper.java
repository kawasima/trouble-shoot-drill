package net.unit8.example.cart.adapter.persistence;

import net.unit8.example.cart.domain.CartItem;
import net.unit8.example.cart.domain.CartItemId;
import net.unit8.example.product.domain.ProductId;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
class CartItemMapper {
    public CartItemJpaEntity domainToEntity(CartItem cartItem) {
        CartItemJpaEntity entity = new CartItemJpaEntity();
        Optional.ofNullable(cartItem.getId())
                .ifPresent(id -> entity.setId(id.getValue()));
        entity.setProductId(cartItem.getProductId().getValue());
        entity.setName(cartItem.getName());
        entity.setAmount(cartItem.getAmount());
        return entity;
    }

    public CartItem entityToDomain(CartItemJpaEntity entity) {
        return CartItem.withId(
                new CartItemId(entity.getId()),
                new ProductId(entity.getProductId()),
                entity.getName(),
                entity.getAmount()
        );
    }
}
