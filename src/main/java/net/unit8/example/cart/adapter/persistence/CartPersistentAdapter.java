package net.unit8.example.cart.adapter.persistence;

import net.unit8.example.cart.application.LoadCartPort;
import net.unit8.example.cart.application.SaveCartPort;
import net.unit8.example.cart.domain.Cart;
import net.unit8.example.stereotype.PersistenceAdapter;
import net.unit8.example.user.domain.MemberId;

import java.util.HashSet;

@PersistenceAdapter
public class CartPersistentAdapter implements SaveCartPort, LoadCartPort {
    private final CartRepository cartRepository;
    private final CartMapper cartMapper;

    public CartPersistentAdapter(CartRepository cartRepository, CartMapper cartMapper) {
        this.cartRepository = cartRepository;
        this.cartMapper = cartMapper;
    }

    @Override
    public Cart load(MemberId memberId) {
        return cartRepository.findByUserId(memberId.getValue())
                .map(cartMapper::entityToDomain)
                .orElseGet(() -> {
                    CartJpaEntity newCart = new CartJpaEntity();
                    newCart.setUserId(memberId.getValue());
                    newCart.setItems(new HashSet<>());
                    cartRepository.save(newCart);
                    return cartMapper.entityToDomain(newCart);
                });
    }

    @Override
    public Cart save(Cart cart) {
        return cartRepository.findByUserId(cart.getMemberId().getValue())
                .map(entity -> {
                    cartMapper.populate(entity, cart);
                    cartRepository.saveAndFlush(entity);
                    return cartMapper.entityToDomain(entity);
                }).orElseThrow(() -> new IllegalArgumentException("Cart not found"));
    }
}
