package net.unit8.example.cart.adapter.persistence;

import net.unit8.example.cart.domain.Cart;
import net.unit8.example.cart.domain.CartId;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
class CartMapper {
    final CartItemMapper cartItemMapper;

    CartMapper(CartItemMapper cartItemMapper) {
        this.cartItemMapper = cartItemMapper;
    }

    public CartJpaEntity domainToEntity(Cart cart) {
        CartJpaEntity entity = new CartJpaEntity();
        Optional.ofNullable(cart.getId())
                .ifPresent(id -> entity.setId(id.getValue()));
        entity.setUserId(cart.getMemberId().getValue());
        entity.setItems(cart.getItems().stream()
                .map(cartItemMapper::domainToEntity)
                .collect(Collectors.toSet()));
        return entity;
    }

    public void populate(CartJpaEntity entity, Cart cart) {
        Set<CartItemJpaEntity> removeItems = new HashSet<>();
        entity.getItems().forEach(item -> {
                    cart.getItem(new ProductId(item.getProductId()))
                            .ifPresentOrElse(
                                    i -> item.setAmount(i.getAmount()),
                                    () -> removeItems.add(item)
                            );
                });
        cart.getItems().forEach(i -> {
            Optional<CartItemJpaEntity> exists = entity.getItems().stream().filter(entityItem -> entityItem.getProductId().equals(i.getProductId().getValue()))
                    .findAny();
            if (exists.isEmpty()) {
                CartItemJpaEntity itemEntity = cartItemMapper.domainToEntity(i);
                itemEntity.setCart(entity);
                entity.getItems().add(itemEntity);
            }
        });
        entity.getItems().removeAll(removeItems);
    }

    public Cart entityToDomain(CartJpaEntity entity) {
        return Cart.withId(new CartId(entity.getId()),
                new MemberId(entity.getUserId()),
                entity.getItems().stream()
                        .map(cartItemMapper::entityToDomain)
                        .collect(Collectors.toSet()));
    }
}
