package net.unit8.example.config;

import net.unit8.example.product.adapter.persistence.ProductJpaEntity;
import net.unit8.example.product.adapter.persistence.ProductRepository;
import net.unit8.example.user.adapter.persistence.MemberJpaEntity;
import net.unit8.example.user.adapter.persistence.MemberRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;

@Component
public class DatabaseInitializer implements InitializingBean {
    private final MemberRepository memberRepository;
    private final ProductRepository productRepository;
    private final TransactionTemplate tx;
    private final PasswordEncoder passwordEncoder;

    public DatabaseInitializer(MemberRepository memberRepository, ProductRepository productRepository, TransactionTemplate tx, PasswordEncoder passwordEncoder) {
        this.memberRepository = memberRepository;
        this.productRepository = productRepository;
        this.tx = tx;
        this.passwordEncoder = passwordEncoder;
    }

    private ProductJpaEntity product(String name, long price) {
        ProductJpaEntity entity = new ProductJpaEntity();
        entity.setName(name);
        entity.setPrice(BigDecimal.valueOf(price));
        return entity;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MemberJpaEntity member1 = new MemberJpaEntity();
        member1.setEmail("member1@example.com");
        member1.setPassword(passwordEncoder.encode("password"));

        tx.execute(status -> {
            memberRepository.save(member1);
            productRepository.save(product("レイピア", 750));
            productRepository.save(product("まけんしのレイピア", 14800));
            productRepository.save(product("てつの大剣", 780));
            productRepository.save(product("キングブレード", 1000));
            productRepository.save(product("どくがのナイフ", 950));
            productRepository.save(product("グラディウス", 13000));
            productRepository.save(product("クロスブーメラン", 750));
            productRepository.save(product("フラワースティック", 750));
            productRepository.save(product("ようせいの杖", 980));
            productRepository.save(product("ウィザードスタッフ", 8300));
            productRepository.save(product("てつのやり", 750));
            productRepository.save(product("聖騎士のやり", 14000));
            productRepository.save(product("バトルリボン", 960));
            productRepository.save(product("魔神のムチ", 10000));
            productRepository.save(product("まじゅうのツメ", 10000));
            productRepository.save(product("はしゃのオノ", 21600));
            productRepository.save(product("シルバートレイ", 270));
            productRepository.save(product("げんまの盾", 12000));
            productRepository.save(product("せいどうの盾", 370));
            productRepository.save(product("ダークシールド", 17200));
            productRepository.save(product("はねぼうし", 280));
            productRepository.save(product("ターバン", 410));
            productRepository.save(product("うさみみバンド", 550));
            productRepository.save(product("神官のぼうし", 7300));
            productRepository.save(product("パピヨンマスク", 8500));
            productRepository.save(product("猛牛ヘルム", 14500));
            productRepository.save(product("皮のドレス", 380));
            productRepository.save(product("レザーマント", 1100));
            productRepository.save(product("おどりこの服", 1300));
            productRepository.save(product("ビロードマント", 9400));
            productRepository.save(product("バンデッドチェイン", 11000));
            productRepository.save(product("しっこくのマント", 11000));
            productRepository.save(product("せいどうのよろい", 840));
            productRepository.save(product("バンデッドメイル", 13000));
            productRepository.save(product("キングコート", 14000));
            productRepository.save(product("クインローブ", 14000));
            return null;
        });
    }
}
