package net.unit8.example.user.domain;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Value;

import java.io.Serializable;

@Value
public class MemberId implements Serializable {
    @JsonValue
    Long value;
}
