package net.unit8.example.user.domain;

import lombok.Value;

import java.io.Serializable;

@Value
public class Password implements Serializable {
    String value;
}
