package net.unit8.example.user.domain;

import lombok.Value;

import java.io.Serializable;

@Value
public class EmailAddress implements Serializable {
    String user;
    String domain;

    public static EmailAddress of(String emailAddress) {
        String[] tokens = emailAddress.split("@");
        if (tokens.length != 2) {
            throw new IllegalArgumentException("Invalid an email address");
        }
        return new EmailAddress(tokens[0], tokens[1]);
    }

    @Override
    public String toString() {
        return user + "@" + domain;
    }
}
