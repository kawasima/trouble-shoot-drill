package net.unit8.example.user.adapter.persistence;

import net.unit8.example.stereotype.PersistenceAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@PersistenceAdapter
public class MemberPersistenceAdapter implements UserDetailsService {
    private final MemberRepository memberRepository;
    private final MemberMapper memberMapper;

    public MemberPersistenceAdapter(MemberRepository memberRepository, MemberMapper memberMapper) {
        this.memberRepository = memberRepository;
        this.memberMapper = memberMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return memberRepository.findByEmail(email)
                .map(memberMapper::entityToDomain)
                .orElseThrow(() -> new UsernameNotFoundException(""));
    }
}
