package net.unit8.example.user.adapter.persistence;

import net.unit8.example.user.domain.EmailAddress;
import net.unit8.example.user.domain.Member;
import net.unit8.example.user.domain.MemberId;
import net.unit8.example.user.domain.Password;
import org.springframework.stereotype.Component;

@Component
public class MemberMapper {
    public MemberJpaEntity domainToEntity(Member member) {
        MemberJpaEntity entity = new MemberJpaEntity();
        entity.setId(member.getId().getValue());
        entity.setEmail(member.getEmail().toString());
        entity.setPassword(member.getPassword());
        return entity;
    }

    public Member entityToDomain(MemberJpaEntity entity) {
        return Member.withId(
                new MemberId(entity.getId()),
                EmailAddress.of(entity.getEmail()),
                new Password(entity.getPassword())
        );
    }
}
