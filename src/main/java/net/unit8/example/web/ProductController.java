package net.unit8.example.web;

import net.unit8.example.cart.application.GetCartCommand;
import net.unit8.example.cart.application.GetCartUseCase;
import net.unit8.example.cart.domain.GotCartEvent;
import net.unit8.example.product.application.SearchProductsCommand;
import net.unit8.example.product.application.SearchProductsUseCase;
import net.unit8.example.product.domain.FoundProductsEvent;
import net.unit8.example.stereotype.WebAdapter;
import net.unit8.example.user.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@WebAdapter
@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private SearchProductsUseCase searchProductsUseCase;

    @Autowired
    private GetCartUseCase getCartUseCase;

    @RequestMapping("/")
    public String index(@RequestParam(value = "q", required = false) String query,
                        @RequestParam(value = "p", defaultValue = "1", required = false) Integer page,
                        @AuthenticationPrincipal Member member,
                        Model model) {
        GotCartEvent gotCartEvent = getCartUseCase.handle(new GetCartCommand(member.getId()));
        model.addAttribute("cartTotalCount", gotCartEvent.getTotalCount());
        FoundProductsEvent event = searchProductsUseCase.handle(new SearchProductsCommand(
                query, page - 1, 10
        ));
        model.addAttribute("products", event.getProducts());
        int totalPages = event.getProducts().getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "product/search";
    }
}
