package net.unit8.example.web;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class UpdateAmountInCartForm {
    private String mode;

    @NotEmpty
    private List<CartItemForm> items;

    @Data
    public static class CartItemForm {
        @NotNull
        @Positive
        private Long productId;

        @NotNull
        @Min(0)
        @Max(100)
        private int amount;
    }
}
