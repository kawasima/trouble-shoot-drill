package net.unit8.example.web;

import net.unit8.example.cart.application.AddItemCommand;
import net.unit8.example.cart.application.AddItemUseCase;
import net.unit8.example.cart.domain.AddedItemEvent;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
@RequestMapping("/cart")
public class CartRestController {
    @Autowired
    private AddItemUseCase addItemUseCase;

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public AddedItemEvent addToCart(@AuthenticationPrincipal Member member, @RequestBody AddItemToCartForm form) {
        return addItemUseCase.handle(new AddItemCommand(
                member.getId(),
                new ProductId(form.getProductId()),
                form.getName(),
                form.getAmount()
        ));
    }
}
