package net.unit8.example.web;

import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.fn.Either;
import net.unit8.example.order.application.*;
import net.unit8.example.order.domain.PlacedOrderEvent;
import net.unit8.example.order.domain.QueriedOrdersEvent;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.stereotype.WebAdapter;
import net.unit8.example.user.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.stream.Collectors;

@WebAdapter
@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private PlaceOrderUseCase placeOrderUseCase;

    @Autowired
    private QueryOrdersUseCase queryOrdersUseCase;

    @PostMapping("place")
    public String place(@Valid PlaceOrderForm placeOrderForm,
                        @AuthenticationPrincipal Member member,
                        Model model,
                        RedirectAttributes redirectAttributes) {
        PlaceOrderCommand command = new PlaceOrderCommand(
                member.getId(),
                placeOrderForm.getOrderLines().stream()
                        .map(form -> new PlaceOrderLineCommand(
                                new ProductId(form.getProductId()),
                                form.getAmount()
                        ))
                        .collect(Collectors.toSet()));
        Either<ConstraintViolations, PlacedOrderEvent> placedOrderResult = placeOrderUseCase.order(command);

        return placedOrderResult.fold(violations -> {
            redirectAttributes.addFlashAttribute("violations", violations);
            return "redirect:/cart/items";
        }, placedOrderEvent -> {
            model.addAttribute("orderId", placedOrderEvent.getOrderId());
            return "order/placed";
        });
    }

    @GetMapping("my")
    public String myOrders(@AuthenticationPrincipal Member member,
                           Model model) {
        QueriedOrdersEvent event = queryOrdersUseCase.handle(new QueryOrdersCommand(
                member.getId()
        ));
        model.addAttribute("orders", event.getOrders());
        return "order/history";
    }
}
