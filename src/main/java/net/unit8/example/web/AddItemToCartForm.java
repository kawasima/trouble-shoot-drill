package net.unit8.example.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class AddItemToCartForm implements Serializable {
    @JsonProperty("product_id")
    private Long productId;
    private String name;
    private Integer amount;
}
