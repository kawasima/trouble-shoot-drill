package net.unit8.example.web;

import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.fn.Either;
import net.unit8.example.cart.application.GetCartCommand;
import net.unit8.example.cart.application.GetCartUseCase;
import net.unit8.example.cart.application.UpdateAmountCommand;
import net.unit8.example.cart.application.UpdateAmountUseCase;
import net.unit8.example.cart.domain.CartItem;
import net.unit8.example.cart.domain.GotCartEvent;
import net.unit8.example.cart.domain.UpdatedAmountEvent;
import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.OrderLine;
import net.unit8.example.product.application.GetProductsCommand;
import net.unit8.example.product.application.GetProductsUseCase;
import net.unit8.example.product.domain.GotProductsEvent;
import net.unit8.example.product.domain.Product;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.stereotype.WebAdapter;
import net.unit8.example.user.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.unit8.example.cart.application.UpdateAmountCommand.amount;

@WebAdapter
@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private GetCartUseCase getCartUseCase;

    @Autowired
    private UpdateAmountUseCase updateAmountUseCase;

    @Autowired
    private GetProductsUseCase getProductsUseCase;

    @GetMapping("/items")
    public String show(@AuthenticationPrincipal Member member,
                       Model model) {
        GotCartEvent event = getCartUseCase.handle(new GetCartCommand(
                member.getId()
        ));
        model.addAttribute("items", event.getItems());
        return "cart/show";
    }

    @PostMapping(value = "/update", params = {"mode=updateAmount"})
    public String updateAmount(@Valid UpdateAmountInCartForm form,
                         @AuthenticationPrincipal Member member,
                         Model model) {
        UpdatedAmountEvent event = updateAmountUseCase.handle(UpdateAmountCommand.create(
                member.getId(),
                form.getItems().stream()
                        .map(item -> amount(new ProductId(item.getProductId()),
                                item.getAmount()))
                        .toArray(UpdateAmountCommand.AmountForUpdate[]::new)));
        model.addAttribute("items", event.getItems());
        return "cart/show";
    }

    @PostMapping(value = "/update", params = {"mode=checkout"})
    public String checkout(@AuthenticationPrincipal Member member,
                           Model model) {
        GotCartEvent gotCartEvent = getCartUseCase.handle(new GetCartCommand(member.getId()));
        Set<ProductId> productIds = gotCartEvent.getItems().stream()
                .map(CartItem::getProductId)
                .collect(Collectors.toSet());
        GotProductsEvent gotProductsEvent = getProductsUseCase.handle(new GetProductsCommand(productIds));
        Map<ProductId, Product> productsInCart = gotProductsEvent.getProducts().stream()
                .collect(Collectors.toMap(Product::getId, p -> p));
        Either<ConstraintViolations, Order> orderResult = Order.withoutId(member.getId(),
                gotCartEvent.getItems().stream()
                        .map(item -> OrderLine.withoutId(
                                item.getProductId(),
                                productsInCart.get(item.getProductId()).getName(),
                                item.getAmount(),
                                productsInCart.get(item.getProductId()).getPrice()
                        ))
                        .collect(Collectors.toSet()));

        return orderResult.fold(violations -> {
            model.addAttribute("items", gotCartEvent.getItems());
            return "cart/show";
        }, order -> {
            model.addAttribute("order", order);
            return "order/confirm";
        });
    }
}
