package net.unit8.example.web;

import net.unit8.example.stereotype.WebAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@WebAdapter
@Controller
public class IndexController {
    @RequestMapping("/")
    public String index() {
        return "redirect:/product/";
    }
}
