package net.unit8.example.web;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Data
public class PlaceOrderForm {
    @NotEmpty
    private List<OrderLineForm> orderLines;

    @Data
    public static class OrderLineForm {
        @NotNull
        @Positive
        private Long productId;

        @NotNull
        @Positive
        private Integer amount;
    }
}
