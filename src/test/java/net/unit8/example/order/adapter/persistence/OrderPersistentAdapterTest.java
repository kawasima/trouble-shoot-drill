package net.unit8.example.order.adapter.persistence;

import am.ik.yavi.core.ConstraintViolations;
import am.ik.yavi.fn.Either;
import net.unit8.example.order.domain.Order;
import net.unit8.example.order.domain.OrderLine;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({OrderPersistentAdapter.class, OrderMapper.class, OrderLineMapper.class})
class OrderPersistentAdapterTest {
    @Autowired
    private OrderPersistentAdapter sut;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void test() {
        Either<ConstraintViolations, Order> orderResult = Order.withoutId(
                new MemberId(1L),
                Set.of(
                        OrderLine.withoutId(new ProductId(1L), "", 3, new BigDecimal("1000")),
                        OrderLine.withoutId(new ProductId(2L), "", 6, new BigDecimal("500")),
                        OrderLine.withoutId(new ProductId(3L), "", 9, new BigDecimal("200"))
                )
        );

        assertThat(orderResult.isRight()).isTrue();
        Order savedOrder = sut.save(orderResult.rightOrElseThrow(v -> new AssertionError("")));
        Order loadedOrder = sut.load(savedOrder.getId());
        assertThat(loadedOrder.getTotalPrice()).isEqualTo(new BigDecimal("7800"));

    }
}