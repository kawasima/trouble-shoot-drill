package net.unit8.example.cart.adapter.persistence;

import net.unit8.example.cart.domain.Cart;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({CartPersistentAdapter.class, CartMapper.class, CartItemMapper.class})
class CartPersistentAdapterTest {
    @Autowired
    private CartPersistentAdapter sut;

    @Sql("CartPersistentAdapterTest.sql")
    @Test
    void test() {
        Cart cart = sut.load(new MemberId(1L));
        cart.addItem(new ProductId(1L), "name1", 10);
        cart.addItem(new ProductId(4L), "name2", 4);
        sut.save(cart);

        Cart updatedCart = sut.load(new MemberId(1L));
        assertThat(updatedCart.getItem(new ProductId(1L)))
                .isNotEmpty()
                .get()
                .hasFieldOrPropertyWithValue("amount", 13);
    }
}