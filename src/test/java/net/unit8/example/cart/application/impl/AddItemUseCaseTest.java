package net.unit8.example.cart.application.impl;

import net.unit8.example.cart.application.AddItemCommand;
import net.unit8.example.cart.application.AddItemUseCase;
import net.unit8.example.cart.application.LoadCartPort;
import net.unit8.example.cart.application.SaveCartPort;
import net.unit8.example.cart.application.impl.AddItemUseCaseImpl;
import net.unit8.example.cart.domain.AddedItemEvent;
import net.unit8.example.cart.domain.Cart;
import net.unit8.example.product.domain.ProductId;
import net.unit8.example.user.domain.MemberId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.transaction.support.TransactionTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

class AddItemUseCaseTest {
    AddItemUseCase sut;

    @BeforeEach
    void setup() {
        LoadCartPort loadCartPort = Mockito.mock(LoadCartPort.class);
        Cart cart = Cart.withoutId(new MemberId(1L));
        cart.addItem(new ProductId(1L), "name", 5);
        Mockito.when(loadCartPort.load(any())).thenReturn(cart);
        SaveCartPort saveCartPort = Mockito.mock(SaveCartPort.class);
        TransactionTemplate tx = new TransactionTemplate();
        sut = new AddItemUseCaseImpl(loadCartPort, saveCartPort, tx);
    }

    @Test
    void test() {
        AddItemCommand command = new AddItemCommand(new MemberId(1L),
                new ProductId(1L),
                "name",
                10);
        AddedItemEvent event = sut.handle(command);
        assertThat(event.getTotalAmount()).isEqualTo(15);
    }
}