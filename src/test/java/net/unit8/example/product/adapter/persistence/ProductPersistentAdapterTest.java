package net.unit8.example.product.adapter.persistence;

import net.unit8.example.product.domain.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import({ProductPersistentAdapter.class, ProductMapper.class})
class ProductPersistentAdapterTest {
    @Autowired
    private ProductPersistentAdapter sut;

    @Test
    @Sql("ProductPersistentAdapterTest.sql")
    void test() {
        Page<Product> products = sut.search("シャ", PageRequest.of(0, 10));
        assertThat(products.getTotalElements()).isEqualTo(1L);
        assertThat(products.toList().get(0)).hasFieldOrPropertyWithValue("name", "シャツ");
    }
}