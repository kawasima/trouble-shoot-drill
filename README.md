# Trouble Shoot Drill

トラブルシューティングのためのドリルです。

## アプリケーションの起動

`net.unit8.example.Application` のmainを起動します。
数秒後、`Tomcat started on port(s): 8080 (http) with context path ''`とログに表示されたら起動完了です。


ブラウザから `http://localhost:8080/` にアクセスしてください。

アカウント: member1@example.com
パスワード: password

でログインできます。

## データベースコンソール

アプリケーション起動後、ブラウザから `http://localhost:8080/h2` にアクセスしてください。

データベースURL:  jdbc:h2:mem:testdb

で接続できます。